import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# Column numbers and names for student survey
def get_names(survey):
    if (survey == 'students'):
        q_number = {'curr_prog': 7,
         'did_BSW': 10,
         'attract_name_BSc': 13,
         'year_BSW': 16,
         'year_MEE': 19,
         'year_MEE2': 22,
         'attract_name_BSW': 28,
         'attract_name_MEE': 31,
         'attract_name_MEE2': 34,
         'miss_name_BSW': 37,
         'miss_name_BSW2': 40,
         'miss_name_MEE': 43,
         'miss_name_MEE2': 46,
         'public_words': 49,
         'recgnize_unique': 54,
         'broad_specific': 59,
         'order_synonym': 62,
         'order_environment': 65,
         'order_earth': 68,
         'which_order': 71,
         'prefix': 74,
         'comment_name': 77,
         'comment_survey': 80}
        q_name = {'curr_prog': 'Programme',
         'did_BSW': 'Studied BSW before MEE',
         'attract_name_BSc': 'What attracted you in the name of your (non- BSW) BSc',
         'year_BSW': 'Year in BSW',
         'year_MEE': 'Year in MEE 1',
         'year_MEE2': 'Year in MEE 2',
         'attract_name_BSW': 'What attracted you in the name BSW',
         'attract_name_MEE': 'What attracted you in the name BSW 1',
         'attract_name_MEE2': 'What attracted you in the name BSW 2',
         'miss_name_BSW': 'What did you miss in the name BSW 1',
         'miss_name_BSW2': 'What did you miss in the name BSW 2',
         'miss_name_MEE': 'What did you miss in the name MEE 1',
         'miss_name_MEE2': 'What did you miss in the name MEE 2',
         'public_words': 'Which words should be included',
         'recgnize_unique': 'Recognizable or unique',
         'broad_specific': 'Broad or specific',
         'order_synonym': 'Order of synonyms',
         'order_environment': 'Order of names with environment',
         'order_earth': 'Order of names with Earth',
         'which_order': 'Which order preferred',
         'prefix': 'Which prefix',
         'comment_name': 'Comment programme name',
         'comment_survey': 'Comment survey'}
    elif (survey == 'staff'):
        # Column numbers and names for staff survey
        q_number = {'chair': 7,
         'teach_in_program': 10,
         'fraction_teaching': 13,
         'fraction_in_program': 16,
         'attract_name_BSW_MEE': 19,
         'miss_name_BSW_MEE': 22,
         'public_words': 25,
         'recgnize_unique': 30,
         'broad_specific': 35,
         'order_synonym': 38,
         'order_environment': 41,
         'order_earth': 44,
         'which_order': 47,
         'prefix': 50,
         'comment_name': 53,
         'comment_survey': 56}
        q_name = {'chair': 'Work in chair group',
         'teach_in_program': 'Teach in programmes',
         'fraction_teaching': 'Fraction of work is teaching',
         'fraction_in_program': 'Fraction of teaching in BSW/MEE',
         'attract_name_BSW_MEE': 'What attracts you in the name BSW',
         'miss_name_BSW_MEE': 'What do you miss in the name BSW/MEE',
         'public_words': 'Which words should be included',
         'recgnize_unique': 'Recognizable or unique',
         'broad_specific': 'Broad or specific',
         'order_synonym': 'Order of synonyms',
         'order_environment': 'Order of names with environment',
         'order_earth': 'Order of names with Earth',
         'which_order': 'Which order preferred',
         'prefix': 'Which prefix',
         'comment_name': 'Comment programme name',
         'comment_survey': 'Comment survey'}
    else:
        print('Do not know survey %s'%(survey))
    return (q_number, q_name)

# Combine some columns
def combine_cols(mydf, ser1_name, ser2_name, sernew_name, my_qnumber, my_qname, description):
    # Add new column
    mydf[sernew_name] = np.NaN
    # Update info dictionaries
    ncols = len(mydf.columns) - 1 # series was added to last column
    q_number.update({sernew_name: ncols})
    q_name.update({sernew_name: description})
    
    ser1 = mydf.iloc[:,q_number[ser1_name]]
    ser2 = mydf.iloc[:,q_number[ser2_name]]
    ser1d = ser1[ser1.notna()]
    ser2d = ser2[ser2.notna()]
    for i in ser1d.index.values:
        mydf.iloc[i, q_number[sernew_name]] = ser1[i]
    for i in ser2d.index.values:
        mydf.iloc[i, q_number[sernew_name]] = ser2[i]
    return mydf, my_qnumber, my_qname

def plot_pies(mydf, vars, q_number, q_name):
    fig, ax = plt.subplots(1, len(vars))
    for i, var in enumerate(vars):
        num_sample = mydf.iloc[:,q_number[var]].count()
        counts = mydf.iloc[:,q_number[var]].value_counts(sort=True)
        counts.sort_index(inplace=True, ascending=False)
        counts.plot(kind="pie", ax=ax[i], label="", figsize=(15,15))
        ax[i].set_title(q_name[var]+" (N=%i)"%(num_sample))
    if (len(vars) > 1):
        fig.tight_layout()
        
def plot_bar(mydf, vars, split_by, q_number, q_name, pref_middle=None, include_all = True):
    split_options = mydf.iloc[:,q_number[split_by]]
    counts = split_options.value_counts()
    split_options = counts.keys().tolist()
    split_options.sort()
    if (include_all):
        split_options.insert(0,'all')     

    fig, ax = plt.subplots(1, len(vars),figsize=(5*len(vars),5))
    if (len(vars) == 1):
        ax=[ax]
    width = 1/(len(split_options)+1)
    for i, var in enumerate(vars):
        for j, prog in enumerate(split_options):
            if (prog == 'all'):
                series = mydf.iloc[:,q_number[var]]
                n_sample = mydf.iloc[:,q_number[split_by]].notna().sum()
            else:
                series = mydf.iloc[:,q_number[var]][mydf.iloc[:,q_number[split_by]] == prog]
                n_sample = len(series)

            counts = series.value_counts()
            if (prog == 'all'):
                counts.sort_index(inplace=True)
                all_keys = counts.keys().tolist()
                if (pref_middle):
                    # Where is the middle one now ?
                    if (len(all_keys) % 2 == 0):
                        print('There is no middle in a list with even number of items')
                    if (pref_middle in all_keys):
                        cur_loc = all_keys.index(pref_middle)
                        mid_loc = int(len(all_keys)/2-0.5)
                        tmp = all_keys[mid_loc]
                        all_keys[mid_loc] = pref_middle
                        all_keys[cur_loc] = tmp
                    else:
                        print('Requested %s not in keys'%(pref_middle))
                    mylist = ['a', 'b', 'c', 'd', 'e']
                    myorder = [3, 2, 0, 1, 4]
                    mylist = [mylist[i] for i in myorder]
                x = np.arange(len(all_keys))
                y = np.zeros(len(all_keys))
            y = y*0
            for k, key in enumerate(all_keys):
                if (key in counts.keys()):
                    y[k] = counts[key]
            n_sample = y.sum()
            ax[i].barh(x + width - j*width, y/n_sample, width, label = prog+' (N=%i)'%(n_sample))
            if (j == len(split_options)-1):
                ax[i].set_yticks(x)
                ax[i].set_yticklabels(all_keys)
                ax[i].set_xlabel('frequency')

    ax[i].legend(bbox_to_anchor=(1, 1), loc='upper left', title=q_name[split_by])
    if (len(vars) > 1):
        fig.tight_layout()
        
def plot_ranking(mydf, orders, split_by, q_number, q_name, include_all = True, transpose=False, do_split=True):
    if (transpose and (len(orders)> 1)):
        print("Can not use transpose when more than one ordering should be plotted")
        return
    if (do_split):
        split_options = mydf.iloc[:,q_number[split_by]]
        counts = split_options.value_counts()
        split_options = counts.keys().tolist()
        split_options.sort()
    else:
        split_options=[]
    
    if (include_all):
        split_options.insert(0,'all')     
    if (transpose):
            plt.figure(figsize=(6*len(split_options),5))
    for k, prog in enumerate(split_options):
        if (not transpose):
            plt.figure(figsize=(6*len(orders),5))
        if (transpose):
            plt.subplot(1,len(split_options),k+1)
        for i, order in enumerate(orders):
            if (not transpose):
                plt.subplot(1,len(orders),i+1)
            # Get the series for this ordering
            if (prog == 'all'):
                set = mydf.iloc[:,q_number[order]]
                n_sample = mydf.iloc[:,q_number[split_by]].notna().sum()
            else:
                set = mydf.iloc[:,q_number[order]][mydf.iloc[:,q_number[split_by]] == prog]
                n_sample = set.shape[0]
            # Split into a series for each option
            set = set.str.split(';',expand=True)
            options = set.iloc[0,:]
            # Drop the empty one (after the last semi colon)
            options.drop(options.index[options==''], inplace=True)
            # Sort so that we always have the same order
            options.sort_index(inplace=True)

            for j, opt in enumerate(options):
                set[opt] = 0
                dummy = np.zeros(set.shape[0], dtype=np.int8)
                for m in range(len(options)):
                    this_rank = (set.iloc[:,m] == options[j])
                    dummy[this_rank] = m+1
                set.iloc[:, set.columns.get_loc(opt)] = dummy

            for j, opt in enumerate(options):
                counts = ((set[options] == j+1).sum())/n_sample
                counts.sort_index(inplace=True, ascending=False)
                if (j == 0):
                    prev_counts = 0*counts
                plt.barh(counts.keys(), counts.values, left = prev_counts.values, label="%i"%(j+1))
                prev_counts += counts
            plt.legend(bbox_to_anchor=(1, 1), loc='upper left', title='Rank', labelspacing=0.3)
            plt.xlim(0,1)
            plt.xlabel('fraction of respondents')
            plt.tight_layout()
            plt.title('%s: %s'%(q_name[split_by],prog))

def extend_names(mydf, myq_number, myq_name, survey='students'):
    if (survey == 'students'):    
        mydf, myq_number, myq_name = combine_cols(mydf, 'year_MEE', 'year_MEE2', 'year_in_MEE_total', 
                                    myq_number, myq_name, 'Year in MEE')
        mydf, myq_number, myq_name = combine_cols(mydf, 'attract_name_MEE', 'attract_name_MEE2', 'attract_name_MEE_total', 
                                    myq_number, myq_name, 'Attractive in name MEE')
        mydf, myq_number, myq_name = combine_cols(mydf, 'miss_name_BSW', 'miss_name_BSW2', 'miss_name_BSW_total', 
                                    myq_number, myq_name, 'Missed in name BSW')
        mydf, myq_number, myq_name = combine_cols(mydf, 'miss_name_MEE', 'miss_name_MEE2', 'miss_name_MEE_total', 
                                    myq_number, myq_name, 'Missed in name MEE')
    elif (survey == 'staff'):
        chairs = [['HWM','SLM'],['MAQ'],['AEW','SOC','SBL','PEN'],['SGL']]
        spec = ['A','B','C','D']
        
        # Add new column
        new_name = 'specialization'
        mydf[new_name] = ''
        # Update info dictionaries
        ncols = len(mydf.columns) - 1 # series was added to last column
        myq_number.update({new_name: ncols})
        myq_name.update({new_name: 'Specialization of chair group'})     

        for i in range(len(spec)):
            for j in range(len(chairs[i])):
                which_rows = mydf.iloc[:,myq_number['chair']].str.contains(chairs[i][j], case=False)
                for k in range(len(which_rows)):
                    if (which_rows[k]):
                        mydf.iloc[k,myq_number['specialization']] = spec[i]
                        
        # Repair case and irregularities in chair
        for i in range(len(mydf)):
            mydf.iloc[i,myq_number['chair']] = mydf.iloc[i,myq_number['chair']].upper()
            if (len(mydf.iloc[i,myq_number['chair']]) > 3):
                mydf.iloc[i,myq_number['chair']] = mydf.iloc[i,myq_number['chair']][:3]
    else:
        print("Unknown survey type: %s"%(survey))
    return (mydf, myq_number, myq_name)
